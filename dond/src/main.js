import Vue from "vue";
import App from "./App.vue";
// import router from "./router";
// import store from "./store";
// import axios from "./vue-axios";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import DOND from "@/components/DOND";
Vue.component("DOND", DOND);

Vue.config.productionTip = false;

// import failApiCall from "@/mixin/failApiCall";
// Vue.mixin(failApiCall);

new Vue({
  // router,
  // store,
  // axios,
  render: h => h(App)
}).$mount("#app");
